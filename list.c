#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

List *makeList()
{
	List *pList;

	pList=(List *)malloc(sizeof(List));
	pList->root=NULL;
	pList->last=NULL;
	pList->num=0;

	return pList;
}

ListNode *makeNode()
{
	ListNode *pNode;
	
	pNode=(ListNode *)malloc(sizeof(ListNode));
	pNode->next=NULL;

	return pNode;
}

void setNode(ListNode *pNode, char *key, char *data)
{
	strcpy(pNode->key,key);
	strcpy(pNode->data,data);
}

void insertNode(List *pList, ListNode *pNode)
{
	if(pList->num!=0)
	{
		pList->last->next=pNode;
		pList->last=pNode;
	}
	else
	{
		pList->root=pList->last=pNode;
	}
	pList->num++;
}

ListNode *getNodeByIndex(List *pList,int index)
{
	ListNode *pNode=pList->root;
	int i;

	for(i=0; i<index; i++)
	{
		pNode=pNode->next;
		if(pNode==NULL)
		{
			return NULL;
		}
	}

	return pNode;
}

void freeList(List *pList)
{
	ListNode *pTemp,*pNode=pList->root;
	
	while(pNode!=NULL)
	{
		pTemp=pNode->next;
		free(pNode);
		pNode=pTemp;
	}
	free(pList);
}

List *json_to_list(char *json, int flag)
{
	List *pList;
	ListNode *pNode;
	char temp [LIST_DATA_SIZE];
	int i, n=0, mode=0;

	pList=makeList();
	for(i=0; i<strlen(json); i++)
	{
		if(mode==2)
		{
			if(json[i]=='"')
			{
				temp[n]='\0';
				pNode=makeNode();
				strcpy(pNode->key,temp);
				insertNode(pList,pNode);
				n=0;
				mode++;
			}
			else
			{
				temp[n]=json[i];
				n++;
			}
		}
		else if(mode==5)
		{
			if(json[i]=='"')
			{
				temp[n]='\0';
				if(flag==LIST_USE_QUOT)
					sprintf(pNode->data,"'%s'",temp);
				else
					strcpy(pNode->data,temp);
				n=0;
				mode++;
			}
			else
			{
				temp[n]=json[i];
				n++;
			}
		}
		else if(mode==7 || mode==8)
		{
			if(json[i]>='0' && json[i]<='9')
			{
				temp[n]=json[i];
				n++;
			}
			else if(json[i]=='.')
			{
				if(mode==7)
				{
					temp[n]=json[i];
					n++;
					mode++;
				}
				else
				{
					freeList(pList);
					return NULL;
				}
			}
			else if(json[i]==',')
			{
				temp[n]='\0';
				strcpy(pNode->data,temp);
				n=0;
				mode=1;
			}
			else if(json[i]==' ' || json[i]=='\t' || json[i]=='\n' || json[i]=='\r')
			{
				temp[n]='\0';
				strcpy(pNode->data,temp);
				n=0;
				mode=6;
			}
			else if(json[i]=='}')
			{
				temp[n]='\0';
				strcpy(pNode->data,temp);
				mode=-1;
			}
			else
			{
				freeList(pList);
				return NULL;
			}
		}
		else if(mode>=0)
		{
			switch(json[i])
			{
			case '{':
				if(mode!=0)
				{
					freeList(pList);
					return NULL;
				}
				mode++;
				break;
			case '}':
				if(mode!=6)
				{
					freeList(pList);
					return NULL;
				}
				mode=-1;
				break;
			case ':':
				if(mode!=3)
				{
					freeList(pList);
					return NULL;
				}
				mode++;
				break;
			case '"':
				if(mode!=1 && mode!=4)
				{
					freeList(pList);
					return NULL;
				}
				mode++;
				break;
			case ',':
				if(mode!=6)
				{
					freeList(pList);
					return NULL;
				}
				mode=1;
				break;
			case ' ': case '\t': case '\n': case '\r':
				break;
			case '0': case '1': case '2': case '3': case '4': case '5': case'6': case '7': case '8': case '9':
				if(mode==4)
				{
					temp[n]=json[i];
					n++;
					mode=7;
					break;
				}
			default:
				freeList(pList);
				return NULL;
			}
		}
		else
		{
			if(json[i]!=' ' && json[i]!='\t' && json[i]!='\n' && json[i]!='\r')
			{
				freeList(pList);
				return NULL;
			}
		}
	}
	if(pList->num==0)
	{
		freeList(pList);
		return NULL;
	}
	return pList;
}
