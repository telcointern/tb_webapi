#define LIST_DATA_SIZE 30
#define LIST_USE_QUOT 1
#define LIST_NOT_USE_QUOT 0

typedef struct ListNode {
	char key[LIST_DATA_SIZE];
	char data[LIST_DATA_SIZE];
	struct ListNode *next;
} ListNode;


typedef struct List {
	ListNode *root;
	ListNode *last;
	int num;
} List;


List *makeList();
ListNode *makeNode();
void setNode(ListNode*,char*,char*);
void insertNode(List *,ListNode *);
ListNode * getNodeByIndex(List *, int);
void freeList(List *);
List *json_to_list(char *json, int flag);

