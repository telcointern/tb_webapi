git clone git@github.com:davidmoreno/onion.git
cd onion
mkdir build
cd build 
cmake ..
make
cd ../..
gcc -c list.c
gcc -o rest rest.c list.o -Ionion/src/ -Lonion/build/src/onion/ -lonion_static -lrt -ltelcobase -I$TELCOBASE_HOME/include/ -L$TELCOBASE_HOME/lib
