﻿1. Onion
	-https://github.com/davidmoreno/onion
	-C언어를 사용하는 웹 프레임워크
	-소스 내 함수를 사용하여 서버 실행
	-main소스 파일 내 URL handlers를 사용하여 url 추가

2. REST

1)정의
	-웹 상의 자료를 HTTP위에서 SOAP이나 쿠키를 통한 세션 트랙킹 같은 별도의 전송 계층 없이 전송하기 위한 아주 간단한 인터페이스
	-복잡한 메시지 없이 URL을 사용하여 데이터를 요청하고, XML이나 JSON 같은 형식으로 데이터를 반환받음
2)구성 요소
	-리소스, 메서드, 메시지
	ex)"이름이 Terry 인 사용자를 생성한다" 라는 호출이 있을 때,
	"사용자"는 생성되는 리소스, "생성한다"라는 행위는 메서드 그리고 "이름이 Terry인 사용자"는 메시지
3)REST의 리소스
	-REST는 리소스 지향 아키텍쳐 스타일로 URL로 표현되는 리소스를 명사로 표현하여 무엇을 나타내는지 알 수 있어야 함
	ex)강남구 사무실의 9층의 HP프린터 -> http://printers/localtion/seoul/kangnamgu/9f/hp
4)REST API 예제
	-다음 API를 사용하여 "'신학기 가방'과 관련된 상품 결과를 최저가순으로 20개씩 3페이지를 JSON으로 받기
	HTTP GET, https://apis.daum.net/shopping/search
	{
		"apikey":"(apikey)",
		"q":"신학기 가방",
		"result":"20",
		"pageno":"3",
		"output":"json",
		"sort":"min_price"
	}
	=> https://apis.daum.net/shopping/search?apikey={apikey}&q=신학기 가방&result=20&pageno=3&output=json&sort=min_price


3. 목표

	-Telcobase의 상태 정보를 REST Service를 사용하여 가져오는 웹 API 서버를 개발
	-서버 상태 정보, 데이터베이스 메모리 정보 등을 요청, 반환

4. request와 response

	-메모리 정보
	GET, /memory
	response {
		"total": "(total)",
		"free": "(free)",
		"allocated": "(allocated)"
	}

	-세션 정보
	GET, /session
	response {
		"session": [
		{
			"session_id": "(session_id1)",
			"session_handle": "(session_handle1)",
			"connected_time":"(connected_time1)",
			"client_ip":"(client_ip1)"
		},
		{
			"session_id": "(session_id2)",
			"session_handle": "(session_handle2)",
			"connected_time":"(connected_time2)",
			"client_ip":"(client_ip2)"
		}],
		"num_sessions": "2"
	}

	-데몬 정보
	GET, /daemon
	response {
		"daemon": [
		{
			"pid": "(pid1)",
			"name": "(name1)"
		},
		{
			"pid": "(pid2)",
			"name": "(name2)"
		}],
		"num_daemons": "2"
	}

	-리플리케이션 상태 정보
	GET, /replication
	response {
		"status": "INVALID_CONFIG"
	}

	-테이블 insert
	POST, /test/user
	request {
		"id": "1",
		"name: "Yeonghun",
		"city": "Incheon",
		"birth": "1991-12-26",
		"score": "10"
	}

	response {
		"insert_rows": "1"
	}

	-테이블 select
	GET, /test/user?id=2
	response {
		"result": [
		{
			"id": "2",
			"name: "Gildong",
			"birth": "1988-01-02"
		}],
		"result_count": "1"
	}

	-테이블 update
	PUT, /test/user?name=Gildong
	request {
		"city":"Incheon"
	}

	response {
		"update_rows":"2"
	}

	-테이블 delete 
	DELETE, /test/user?id=1
	response {
		"delete_rows":"1"
	}