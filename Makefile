CC=gcc
CFLAGS=
OBJS=list.o
LIBS=-Ionion/src/ -Lonion/build/src/onion/ -lonion_static -lrt -ltelcobase -I$(TELCOBASE_HOME)/include/ -L$(TELCOBASE_HOME)/lib
all: rest

rest:	rest.c $(OBJS)
	$(CC) $(CFLAGS) -g -o rest rest.c $(OBJS) $(LIBS)

list.o:	list.c
	$(CC) $(CFLAGS) -c list.c

clean:
	rm -f rest
	rm -f *.o
