#include <onion/onion.h>
#include <onion/log.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <dal.h>

#include "list.h"

onion *o=NULL;
DAL_CONN *conn=NULL;
DAL_PSTMT *stmt=NULL;

// define handler
onion_connection_status memory_handler(void *, onion_request *, onion_response *);
onion_connection_status session_handler(void *, onion_request *, onion_response *);
onion_connection_status daemon_handler(void *, onion_request *, onion_response *);
onion_connection_status replication_handler(void *, onion_request *, onion_response *);

onion_connection_status table_sys_tables_handler(void *, onion_request *, onion_response *);
onion_connection_status table_desc_user_handler(void *, onion_request *, onion_response *);

onion_connection_status table_user_handler(void *, onion_request *, onion_response *);

onion_connection_status table_rawbuffer_user_handler(void *, onion_request *, onion_response *);
onion_connection_status table_prepared_user_handler(void *, onion_request *, onion_response *);
// define function
int get_table_select(char *, onion_request *, onion_response *);
int post_table_insert(char *, onion_request *, onion_response *);
int put_table_update(char *, onion_request *, onion_response *);
int delete_table_delete(char *, onion_request *, onion_response *);
int result_to_json_write(onion_response *,DAL_RESULT_SET *);

static void shutdown_server(int _)
{
	DAL_CONN *tmp_conn=conn;
	if(tmp_conn)
	{
		conn=NULL;
		(void)dalDisconnect(tmp_conn);
	}
	if(o)
		onion_listen_stop(o);
}

// main function
int main(int argc, char **argv)
{
	char query[256];

	// connect telcobase
	conn=dalConnect(NULL);
	if(conn==NULL)
	{
		fprintf(stderr,"[TELCOBASE]can't connect to database, errno=%d\n",errno);
		exit(-1);
	}
	if(dalSetAutoCommit(conn,DAL_ON)<0)
	{
		fprintf(stderr,"[TELCOBASE]can't set auto commit mode, errno=%d\n",errno);
		exit(-1);
	}
	// make Prepared Statement query
	tb_strcpy(query,"update user set city=?city, score=?score where id=?id");
	stmt=dalPreparedStatement(conn,query);
	if(stmt==NULL)
	{
		fprintf(stderr,"[TECOBASE]can't make a Prepared Statement, query=%s errno=%d",query,errno);
		exit(-1);
	}
	o=onion_new(O_ONE_LOOP);

	tb_signal(SIGINT,shutdown_server);
	tb_signal(SIGTERM,shutdown_server);
	tb_signal(SIGQUIT,shutdown_server);
	tb_signal(SIGHUP,shutdown_server);

	onion_url *urls=onion_root_url(o);

	// add url
	onion_url_add_static(urls, "", "Telcobase REST API", HTTP_OK);

	onion_url_add(urls, "^memory/?$", memory_handler);
	onion_url_add(urls, "^session/?$", session_handler);
	onion_url_add(urls, "^daemon/?$", daemon_handler);
	onion_url_add(urls, "^replication/?$", replication_handler);

	onion_url_add(urls, "^table/__sys_tables__/?$", table_sys_tables_handler);
	onion_url_add(urls, "^table/desc/user/?$", table_desc_user_handler);

	onion_url_add(urls, "^table/user/?$", table_user_handler);

	onion_url_add(urls, "^table/rawbuffer/user/?$", table_rawbuffer_user_handler);
	onion_url_add(urls, "^table/prepared/user/?$", table_prepared_user_handler);

	// start server
	onion_listen(o);
	onion_free(o);

	if(dalDestroyPreparedStmt(stmt) < 0)
	{
		fprintf(stderr,"[TELCOBASE]can't destroy a Prepared Statement, errno=%d\n",dalErrno());
		exit(-1);
	}

	return 0;
}

onion_connection_status memory_handler(void *p, onion_request *req, onion_response *res)
{
	DAL_MEM_INFO *minfo;
	int method;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_GET)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// get memory information
	if((minfo=dalGetMemInfo(conn))==NULL)
	{
		fprintf(stderr,"[TELCOBASE]can't get memory information, errno=%d\n",errno);
		onion_response_printf(res,"[TELCOBASE]can't get memory information, errno=%d\n",errno);
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	// set content-type
	onion_response_set_header(res,"content-type","application/json");

	onion_response_printf(res, "{\n\t\"total\": \"%s\",\n\t\"free\": \"%s\",\n\t\"allocated\": \"%s\"\n}",minfo->total,minfo->free,minfo->allocated);

	return OCS_PROCESSED;
}

onion_connection_status session_handler(void *p, onion_request *req, onion_response *res)
{
	DAL_SESSION_INFO *sinfo;
	struct _dal_session *ds;
	char temp[200]={0};
	int method;
	int i;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_GET)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// get session information
	if((sinfo=dalGetSessionInfo(conn))==NULL)
	{
		fprintf(stderr,"[TELCOBASE]can't get session information, errno=%d\n",errno);
		onion_response_printf(res,"[TELCOBASE]can't get session information, errno=%d\n",errno);
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	if(sinfo->num_sessions==0)
	{
		onion_response_write0(res,"No session");
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	// set content-type
	onion_response_set_header(res,"content-type","application/json");

	onion_response_printf(res,"{\n\t\"num_sessions\": %d,\n\t\"session\": [\n\t\t",sinfo->num_sessions);
	for(i=0;i<sinfo->num_sessions;i++)
	{
		if(i!=0)
		{
			onion_response_printf(res,",\n\t\t");
		}
		ds=sinfo->session_p+i;
		sprintf(temp,"{ \"session_id\": \"%s\", \"session_handle\": \"%s\", \"connected_time\": \"%s\", \"client_ip\": \"%s\" }",
			ds->session_id,ds->session_handle,ds->connected_time,ds->client_ip);
		onion_response_printf(res,"%s",temp);
	}
	onion_response_printf(res,"]\n}");

	return OCS_PROCESSED;
}

onion_connection_status daemon_handler(void *p, onion_request *req, onion_response *res)
{
	DAL_DAEMONS dinfo;
	char temp[200]={0};
	int method;
	int i,num_daemons;
	int t;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_GET)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// get daemons alive
	if((num_daemons=dalGetDaemonsAlive(conn,&dinfo))<0)
	{
		fprintf(stderr,"[TELCOBASE]can't get information of alive daemons, errno=%d\n",errno);
		onion_response_printf(res,"[TELCOBASE]can't get information of alive daemons, errno=%d\n",errno);
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	if(num_daemons==0)
	{
		onion_response_write0(res,"No daemon");
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	// set content-type
	onion_response_set_header(res,"content-type","application/json");

	onion_response_printf(res,"{\n\t\"num_daemons\": %d,\n\t\"daemon\": [\n\t\t",num_daemons);
	for(i=0;i<num_daemons;i++)
	{
		if(i!=0)
		{
			onion_response_printf(res,",\n\t\t");
		}
		t=sprintf(temp,"{ \"pid\": \"%d\", \"name\": \"%s\" }",dinfo[i].pid,dinfo[i].name);
		onion_response_printf(res,"%s",temp);
	}
	onion_response_printf(res,"]\n}");

	return OCS_PROCESSED;
}

onion_connection_status replication_handler(void *p, onion_request *req, onion_response *res)
{
	int method;
	int status;
	char mesg[20];

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_GET)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// get replication status
	status=dalGetReplicationStatus(conn);

	if(status==DAL_SUCCESS)
		strcpy(mesg,"SUCCESS");
	else if(status==DAL_FAILURE)
		strcpy(mesg,"FAILURE");
	else if(status==DAL_INVALID_CONFIG)
		strcpy(mesg,"INVALID_CONFIG");
	else
		strcpy(mesg,"EXECUTION_ERROR");
	// set content-type
	onion_response_set_header(res,"content-type","application/json");
	onion_response_printf(res, "{\n\t\"status\": \"%s\"\n}",mesg);

	return OCS_PROCESSED;
}

onion_connection_status table_sys_tables_handler(void *p, onion_request *req, onion_response *res)
{
	DAL_RESULT_SET *result=NULL;
	char query[256];
	int method;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_GET)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// main operation
	tb_strcpy(query,"select table_name,table_type,numtuples,loglevel from __sys_tables__");
	result=dalExecQuery(conn,query);
	if(result==NULL)
	{
		fprintf(stderr,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
		onion_response_printf(res,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	result_to_json_write(res,result);

	// set content-type
	onion_response_set_header(res,"content-type","application/json");
	dalResFree(result);

	return OCS_PROCESSED;
}

onion_connection_status table_desc_user_handler(void *p, onion_request *req, onion_response *res)
{
	char table[5]="user";
	DAL_RESULT_SET *result=NULL;
	DAL_ENTRY *entry=NULL;
	char query[256], temp[100];
	int method, i, len;
	int result_count;
	DAL_STRING col_name, type_name;
	DAL_INT col_size;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_GET)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// main operation
	sprintf(query,"select column_name,data_type,type_name,column_size from __sys_columns__ where table_name='%s'",table);
	result=dalExecQuery(conn,query);
	if(result==NULL)
	{
		fprintf(stderr,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
		onion_response_printf(res,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	//result_to_json_write(res,result);
	
	result_count=dalGetResultSetSize(result);
	if(result_count==0)
	{
		onion_response_write0(res,"No column");
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	entry=dalFetchFirst(result);
	onion_response_write0(res,"{\n\t");
	for(i=0;i<result_count;i++)
	{
		if(i!=0)
		{
			onion_response_write0(res,",\n\t");
		}
		if(dalGetStringByKey(entry,"COLUMN_NAME",&col_name) < 0)
		{
			fprintf(stderr,"[TELCOBASE]dalGetStringByKey() error\n");
			col_name=DAL_STRING_NULL;
		}
		if(dalGetStringByKey(entry,"TYPE_NAME",&type_name) < 0)
		{
			fprintf(stderr,"[TELCOBASE]dalGetStringByKey() error\n");
			type_name=DAL_STRING_NULL;
		}
		len=sprintf(temp,"\"%s\": \"%s\"",col_name,type_name);
		if(strcmp(type_name,"CHAR()")==0)
		{
			if(dalGetIntByKey(entry,"COLUMN_SIZE",&col_size) < 0)
			{
				fprintf(stderr,"[TELCOBASE]dalGetIntByKey error\n");
			}
			else
			{
				sprintf(temp+(len-2),"%d)\"",col_size);
			}
		}
		onion_response_write0(res,temp);
		entry=dalFetchNext(result);
	}
	onion_response_write0(res,"\n}");

	// set content-type
	onion_response_set_header(res,"content-type","application/json");
	dalResFree(result);

	return OCS_PROCESSED;
}

onion_connection_status table_user_handler(void *p, onion_request *req, onion_response *res)
{
	char table[5]="user";
	int r, method;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method==OR_GET)
	{
		r=get_table_select(table,req,res);
		if(r==-1)
		{
			onion_response_set_code(res,HTTP_BAD_REQUEST);
			return OCS_KEEP_ALIVE;
		}
	}
	else if(method==OR_POST)
	{
		r=post_table_insert(table,req,res);
		if(r==-1)
		{
			onion_response_set_code(res,HTTP_BAD_REQUEST);
			return OCS_KEEP_ALIVE;
		}
	}
	else if(method==OR_PUT)
	{
		r=put_table_update(table,req,res);
		if(r==-1)
		{
			onion_response_set_code(res,HTTP_BAD_REQUEST);
			return OCS_KEEP_ALIVE;
		}
	}
	else if(method==OR_DELETE)
	{
		r=delete_table_delete(table,req,res);
		if(r==-1)
		{
			onion_response_set_code(res,HTTP_BAD_REQUEST);
			return OCS_KEEP_ALIVE;
		}
	}
	else
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// set content-type
	onion_response_set_header(res,"content-type","application/json");

	return OCS_PROCESSED;
}

onion_connection_status table_rawbuffer_user_handler(void *p, onion_request *req, onion_response *res)
{
	DAL_RESULT_SET *result=NULL;
	DAL_ENTRY *entry=NULL;
	char query[256], temp[LIST_DATA_SIZE];
	int method, size, i;
	size_t result_count=0;
	
	struct st_user {
		int id;
		char name[31];
		char city[31];
		long long birth;
		double score;
	} *user;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_GET)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// main operation
	tb_strcpy(query,"select * from user");
	result=dalExecQuery(conn,query);
	if(result==NULL)
	{
		fprintf(stderr,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
		onion_response_printf(res,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
		onion_response_set_code(res,HTTP_BAD_REQUEST);
		return OCS_KEEP_ALIVE;
	}
	result_count=dalGetResultSetSize(result);
	if(result_count==0)
	{
		onion_response_printf(res,"{\n\t\"result_count\": 0\n}");
	}
	else
	{
		entry=dalFetchFirst(result);
		onion_response_printf(res,"{\n\t\"result_count\": %d,\n\t\"result\": [\n\t\t",result_count);
		for(i=0;i<result_count;i++)
		{
			if(i!=0)
			{
				onion_response_printf(res,",\n\t\t{ ");
			}
			else
			{
				onion_response_printf(res,"{ ");
			}
			// get Raw Data Buffer
			user=(struct st_user *)dalGetRawDataBuffer(entry,&size);
			onion_response_printf(res,"\"id\": %d",user->id);
				onion_response_printf(res,", \"name\": \"%s\"",user->name);
			if(strlen(user->city)!=0)
			{
				onion_response_printf(res,", \"city\": \"%s\"",user->city);
			}
			if(user->birth!=0)
			{
				if(dalDatetime2Str(temp,sizeof(temp),user->birth,DAL_TYPE_DATE) < 0)
				{
					fprintf(stderr,"[TELCOBASE]dalDatetime2Str() error\n");
				}
				else
				{
					temp[10]='\0';
					onion_response_printf(res,", \"birth\": \"%s\"",temp);
				}
			}
			if(user->score!=DAL_DOUBLE_NULL)
			{
				onion_response_printf(res,", \"score\": %lf",user->score);
			}
			onion_response_write0(res," }");
			entry=dalFetchNext(result);
		}
	}
	onion_response_printf(res,"]\n}");

	// set content-type
	onion_response_set_header(res,"content-type","application/json");
	dalResFree(result);

	return OCS_PROCESSED;
}

onion_connection_status table_prepared_user_handler(void *p, onion_request *req, onion_response *res)
{
	DAL_STRING query;
	char *temp=NULL, *filename, *data;
	DAL_INT id;
	DAL_DOUBLE score;
	onion_dict *od=NULL;
	onion_block *ob=NULL;
	List *pList;
	ListNode *pNode;
	int method, fd, n, update_rows;

	// check method
	method=onion_request_get_flags(req)&OR_METHODS;
	if(method!=OR_PUT)
	{
		onion_response_write0(res,"Wrong method");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}

	ob=onion_request_get_data(req);
	filename=onion_block_data(ob);
	fd=open(filename,O_RDONLY);
	n=lseek(fd,0,SEEK_END);
	if(n==0)
	{
		onion_response_write0(res,"Request body is empty");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	data=(char*)malloc((sizeof(char)*n)+1);
	lseek(fd,0,SEEK_SET);
	read(fd,data,n);
	data[n]='\0';
	pList=json_to_list(data,LIST_NOT_USE_QUOT);
	free(data);
	if(pList==NULL)
	{
		onion_response_write0(res,"Wrong request body data(json)");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	temp=onion_request_get_query(req,"id");
	if(temp==NULL)
	{
		onion_response_write0(res,"Wrong key of parameter(id)");
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	id=atoi(temp);
	
	query=dalPreparedGetQuery(stmt);
	if(query==NULL)
	{
		fprintf(stderr,"[TECOBASE]can't get the query string from Prepared Statement, errno=%d",errno);
		onion_response_printf(res,"[TECOBASE]can't get the query string from Prepared Statement, errno=%d",errno);
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}

	for(pNode=pList->root; pNode!=NULL; pNode=pNode->next)
	{
		if(strcmp("city",pNode->key)==0)
		{
			if(dalSetStringByKey(stmt,"city",pNode->data)<0)
			{
				fprintf(stderr,"[TECOBASE]can't set string value, query=%s errno=%d",query,errno);
				onion_response_printf(res,"[TECOBASE]can't set string value, query=%s errno=%d",query,errno);
				onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
				return OCS_KEEP_ALIVE;
			}
		}
		else if(strcmp("score",pNode->key)==0)
		{
			score=atof(pNode->data);
			if(dalSetDoubleByKey(stmt,"score",&score)<0)
			{
				fprintf(stderr,"[TECOBASE]can't set double value, query=%s errno=%d",query,errno);
				onion_response_printf(res,"[TECOBASE]can't set double value, query=%s errno=%d",query,errno);
				onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
				return OCS_KEEP_ALIVE;
			}
		}
		else
		{
			onion_response_write0(res,"Wrong key of body data(city or score)");
			onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
			return OCS_KEEP_ALIVE;
		}
	}
	if(dalSetIntByKey(stmt,"id",id)<0)
	{
		fprintf(stderr,"[TECOBASE]can't set int value, query=%s errno=%d",query,errno);
		onion_response_printf(res,"[TECOBASE]can't set int value, query=%s errno=%d",query,errno);
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	// execute a Prepared Statement
	if((update_rows=dalPreparedExecUpdate(conn,stmt))<0)
	{
		fprintf(stderr,"[TECOBASE]can't execute a Prepared Statement, errno=%d",errno);
		onion_response_printf(res,"[TECOBASE]can't execute a Prepared Statement, errno=%d",errno);
		onion_response_set_code(res,HTTP_METHOD_NOT_ALLOWED);
		return OCS_KEEP_ALIVE;
	}
	onion_response_printf(res,"{\n\t\"update_rows\": %d\n}",update_rows);

	// set content-type
	onion_response_set_header(res,"content-type","application/json");
	return OCS_PROCESSED;
}
int get_table_select(char *table, onion_request *req, onion_response *res)
{
	DAL_RESULT_SET *result=NULL;
	DAL_ENTRY *entry=NULL;
	DAL_STRING key;
	char *data;
	char query[256];
	onion_dict *od=NULL;
	int type;

	od=onion_request_get_query_dict(req);
	if(onion_dict_count(od)==0)
	{
		sprintf(query,"select * from %s",table);
		result=dalExecQuery(conn,query);
		if(result==NULL)
		{
			fprintf(stderr,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
			onion_response_printf(res,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
			return -1;
		}
		// make json from result set
		result_to_json_write(res,result);
	}
	else if(onion_dict_count(od)==1)
	{
		sprintf(query,"select column_name,data_type from __sys_columns__ where table_name='%s'",table);
		result=dalExecQuery(conn,query);
		for(entry=dalFetchFirst(result); entry!=NULL; entry=dalFetchNext(result))
		{
			if(dalGetStringByIndex(entry,1,&key) < 0)
			{
				fprintf(stderr,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n",errno);
				onion_response_printf(res,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n", errno);
				dalResFree(result);
				return -1;
			}
			if((data=onion_dict_get(od,key))!=NULL)
			{
				if(dalGetIntByIndex(entry,2,&type)<0)
				{
					fprintf(stderr,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n",errno);
					onion_response_printf(res,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n", errno);
					dalResFree(result);
					return -1;
				}
				break;
			}
		}
		if(entry==NULL)
		{
			onion_response_write0(res,"Wrong key of parameter");
			dalResFree(result);
			return -1;
		}
		// add where clause
		if((type>=1) && (type<=3) || (type>=9))
			sprintf(query,"select * from %s where %s='%s'",table,key,data);
		else
			sprintf(query,"select * from %s where %s=%s",table,key,data);
		// execute query
		result=dalExecQuery(conn,query);
		if(result==NULL)
		{
			fprintf(stderr,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
			onion_response_printf(res,"[TELCOBASE]can't execute a select query, query=%s errno=%d\n",query,errno);
			return -1;
		}
		// make json from result set
		result_to_json_write(res,result);
	}
	else
	{
		onion_response_write0(res,"Wrong number of parameter(1 of 0)");
		return -1;
	}
	dalResFree(result);
	return 0;
}

int post_table_insert(char *table, onion_request *req, onion_response *res)
{
	char *data;
	char query[256];
	onion_block *ob=NULL;
	int num_columns, insert_rows;
	int i, len;
	List *pList;
	ListNode *pNode;

	// parse request data
	ob=onion_request_get_data(req);
	if(ob==NULL)
	{
		onion_response_write0(res,"Request body is empty");
		return -1;
	}
	data=onion_block_data(ob);
	// json data to List
	pList=json_to_list(data,LIST_USE_QUOT);
	if(pList==NULL)
	{
		onion_response_write0(res,"Wrong request body data(json)");
		return -1;
	}
	// make insert query
	len=sprintf(query,"insert into %s(",table);
	pNode=pList->root;
	for(i=0;i<pList->num;i++)
	{
		if(i!=0)
			len+=sprintf(query+len,", %s",pNode->key);
		else
			len+=sprintf(query+len,"%s",pNode->key);
		pNode=pNode->next;
	}
	len+=sprintf(query+len,") values(");
	pNode=pList->root;
	for(i=0;i<pList->num;i++)
	{
		if(i!=0)
			len+=sprintf(query+len,", %s",pNode->data);
		else
			len+=sprintf(query+len,"%s",pNode->data);
		pNode=pNode->next;
	}
	sprintf(query+len,")");
	// execute query
	if((insert_rows=dalExecUpdate(conn,query)) < 0)
	{
		fprintf(stderr,"[TELCOBASE]can't execute a update query, query=%s errno=%d\n",query,dalErrno());
		onion_response_printf(res,"[TELCOBASE]can't execute a update query, query=%s errno=%d\n",query,dalErrno());
		freeList(pList);
		return -1;
	}
	onion_response_printf(res,"{\n\t\"insert_rows\": %d\n}",insert_rows);

	freeList(pList);
	return 0;
}

int put_table_update(char *table, onion_request *req, onion_response *res)
{
	DAL_RESULT_SET *result=NULL;
	DAL_ENTRY *entry=NULL;
	DAL_STRING key=NULL;
	char *data, *filename;
	char query[256], query2[256];
	onion_dict *od=NULL;
	onion_block *ob=NULL;
	int type, update_rows;
	List *pList;
	ListNode *pNode;
	int i, len, fd, n;

	ob=onion_request_get_data(req);
	// read temp file name
	filename=onion_block_data(ob);
	fd=open(filename,O_RDONLY);
	n=lseek(fd,0,SEEK_END);
	if(n==0)
	{
		onion_response_write0(res,"Request body is empty");
		return -1;
	}
	data=(char*)malloc((sizeof(char)*n)+1);
	lseek(fd,0,SEEK_SET);
	// read body data in temp file
	read(fd,data,n);
	data[n]='\0';
	// json data to List
	pList=json_to_list(data,LIST_USE_QUOT);
	free(data);
	if(pList==NULL)
	{
		onion_response_write0(res,"Wrong request body data(json)");
		return -1;
	}

	// make update query
	len=sprintf(query,"update %s set ",table);
	pNode=pList->root;
	for(i=0;i<pList->num;i++)
	{
		if(i==0)
			len+=sprintf(query+len,"%s=%s",pNode->key,pNode->data);
		else
			len+=sprintf(query+len,", %s=%s",pNode->key,pNode->data);
		pNode=pNode->next;
	}
	freeList(pList);

	od=onion_request_get_query_dict(req);
	if(onion_dict_count(od)==0)
	{
		if((update_rows=dalExecUpdate(conn,query)) < 0)
		{
			fprintf(stderr,"[TELCOBASE]can't execute a update query, query %s errno=%d\n",query,dalErrno());
			onion_response_printf(res,"[TELCOBASE]can't execute a update query, query %s errno=%d\n",query,dalErrno());
			free(key);
			return -1;		
		}
		onion_response_printf(res,"{\n\t\"update_rows\": %d\n}",update_rows);
		
	}
	else if(onion_dict_count(od)==1)
	{
		sprintf(query2,"select column_name,data_type from __sys_columns__ where table_name='%s'",table);
		result=dalExecQuery(conn,query2);
		for(entry=dalFetchFirst(result); entry!=NULL; entry=dalFetchNext(result))
		{
			if(dalGetStringByIndex(entry,1,&key) < 0)
			{
				fprintf(stderr,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n",errno);
				onion_response_printf(res,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n", errno);
				dalResFree(result);
				return -1;
			}
			if((data=onion_dict_get(od,key))!=NULL)
			{
				if(dalGetIntByIndex(entry,2,&type)<0)
				{
					fprintf(stderr,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n",errno);
					onion_response_printf(res,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n", errno);
					dalResFree(result);
					return -1;
				}
				break;
			}
		}
		if(entry==NULL)
		{
			onion_response_write0(res,"Wrong key of parameter");
			dalResFree(result);
			return -1;
		}
		dalResFree(result);
		// add where clause
		if((type>=1) && (type<=3) || (type>=9))
			sprintf(query+len," where %s='%s'",key,data);
		else
			sprintf(query+len," where %s=%s",key,data);

		if((update_rows=dalExecUpdate(conn,query)) < 0)
		{
			fprintf(stderr,"[TELCOBASE]can't execute a update query, query %s errno=%d\n",query,dalErrno());
			onion_response_printf(res,"[TELCOBASE]can't execute a update query, query %s errno=%d\n",query,dalErrno());
			return -1;		
		}
		onion_response_printf(res,"{\n\t\"update_rows\": %d\n}",update_rows);
	}
	else
	{
		onion_response_write0(res,"Wrong number of parameter(1 of 0)");
		return -1;
	}

	return 0;
}
int delete_table_delete(char *table, onion_request *req, onion_response *res)
{
	DAL_RESULT_SET *result=NULL;
	DAL_ENTRY *entry=NULL;
	DAL_STRING key=NULL;
	char *data;
	char query[256];
	onion_dict *od=NULL;
	int type, delete_rows;

	// get parameter
	od=onion_request_get_query_dict(req);
	if(onion_dict_count(od)==0)
	{
		sprintf(query,"delete from %s",table);
		if((delete_rows=dalExecUpdate(conn,query)) < 0)
		{
			fprintf(stderr,"[TELCOBASE]can't execute a delete query, query %s errno=%d\n",query,dalErrno());
			onion_response_printf(res,"[TELCOBASE]can't execute a delete query, query %s errno=%d\n",query,dalErrno());
			return -1;		
		}
		onion_response_printf(res,"{\n\t\"delete_rows\": %d\n}",delete_rows);
		
	}
	else if(onion_dict_count(od)==1)
	{
		sprintf(query,"select column_name,data_type from __sys_columns__ where table_name='%s'",table);
		result=dalExecQuery(conn,query);
		for(entry=dalFetchFirst(result); entry!=NULL; entry=dalFetchNext(result))
		{
			if(dalGetStringByIndex(entry,1,&key) < 0)
			{
				fprintf(stderr,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n",errno);
				onion_response_printf(res,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n", errno);
				dalResFree(result);
				return -1;
			}
			if((data=onion_dict_get(od,key))!=NULL)
			{
				if(dalGetIntByIndex(entry,2,&type)<0)
				{
					fprintf(stderr,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n",errno);
					onion_response_printf(res,"[TELCOBASE]can't get a integer value from result entry, errno=%d\n", errno);
					dalResFree(result);
					return -1;
				}
				break;
			}
		}
		if(entry==NULL)
		{
			onion_response_write0(res,"Wrong key of parameter");
			dalResFree(result);
			return -1;
		}
		dalResFree(result);
		// add where clause
		if((type>=1) && (type<=3) || (type>=9))
			sprintf(query,"delete from %s where %s='%s'",table,key,data);
		else
			sprintf(query,"delete from %s where %s=%s",table,key,data);
		// execute query
		if((delete_rows=dalExecUpdate(conn,query)) < 0)
		{
			fprintf(stderr,"[TELCOBASE]can't execute a delete query, query %s errno=%d\n",query,dalErrno());
			onion_response_printf(res,"[TELCOBASE]can't execute a delete query, query %s errno=%d\n",query,dalErrno());
			return -1;		
		}
		onion_response_printf(res,"{\n\t\"delete_rows\": %d\n}",delete_rows);
	}
	else
	{
		onion_response_write0(res,"Wrong number of parameter(1 of 0)");
		return -1;
	}

	return 0;
}

int result_to_json_write(onion_response *res, DAL_RESULT_SET *result)
{
	DAL_ENTRY *entry;	
	char query[256];
	char **key;
	int *type;
	size_t result_count=0;
	int num_columns;

	DAL_INT d_int;
	DAL_LONG d_long;
	DAL_DOUBLE d_double;
	//DAL_NUMERIC d_numeric;
	DAL_STRING d_string;
	DAL_DATETIME d_datetime;
	//DAL_OCTETS d_octets;
	int i,j;
	char temp[LIST_DATA_SIZE];

	result_count=dalGetResultSetSize(result);
	if(result_count==0)
	{
		onion_response_printf(res,"{\n\t\"result_count\": 0\n}");
		return 0;
	}
	
        num_columns=dalGetResultSetNumAttrs(result);

	key=(char**)malloc(sizeof(char *)*num_columns);
	type=(int *)malloc(sizeof(int)*num_columns);
	entry=dalFetchFirst(result);
        for(i=1;i<=num_columns;i++)
        {
                key[i-1]=dalGetAttrNameByIndex(result,i);
		type[i-1]=dalGetAttrTypeByIndex(entry,i);
        }
	// make json
	onion_response_printf(res,"{\n\t\"result_count\": %d,\n\t\"result\": [\n\t\t",result_count);
	for(j=0;j<result_count;j++)
	{
		if(j!=0)
		{
			onion_response_printf(res,",\n\t\t{ ");
		}
		else
		{
			onion_response_printf(res,"{ ");
		}
		for(i=0;i<num_columns;i++)
		{
			// check data type
			if(type[i]==DAL_ATTR_INT)
			{
				if(dalGetIntByKey(entry,key[i],&d_int) < 0)
				{
					fprintf(stderr,"[TELCOBASE]dalGetIntByKey() error\n");
					continue;
				}
				if(d_int==DAL_INT_NULL)
					continue;
				if(i!=0)
					onion_response_printf(res,", ");
				onion_response_printf(res,"\"%s\": %d",key[i],d_int);
			}
			else if(type[i]==DAL_ATTR_LONG)
			{
				if(dalGetLongByKey(entry,key[i],&d_long) < 0)
				{
					fprintf(stderr,"[TELCOBASE]dalGetLongByKey() error\n");
					continue;
				}
				if(d_long==DAL_LONG_NULL)
					continue;
				if(i!=0)
					onion_response_printf(res,", ");
				onion_response_printf(res,"\"%s\": %lld",key[i],d_long);
			}
			else if(type[i]==DAL_ATTR_DOUBLE)
			{
				if(dalGetDoubleByKey(entry,key[i],&d_double) < 0)
				{
					fprintf(stderr,"[TELCOBASE]dalGetDoubleByKey() error\n");
					continue;
				}
				if(d_double==DAL_DOUBLE_NULL)
					continue;
				if(i!=0)
					onion_response_printf(res,", ");
				onion_response_printf(res,"\"%s\": %lf",key[i],d_double);
			}
			else if(type[i]==DAL_ATTR_STRING)
			{
				if(dalGetStringByKey(entry,key[i],&d_string) < 0)
				{
					fprintf(stderr,"[TELCOBASE]dalGetStringByKey() error\n");
					continue;
				}
				if(d_string==DAL_STRING_NULL)
					continue;
				if(i!=0)
					onion_response_printf(res,", ");
				onion_response_printf(res,"\"%s\": \"%s\"",key[i],d_string);
			}
			else if((type[i]==DAL_ATTR_DATE) || (type[i]==DAL_ATTR_DATETIME)) 
			{
				if(dalGetDatetimeByKey(entry,key[i],&d_datetime) < 0)
				{
					fprintf(stderr,"[TELCOBASE]dalGetDatetimeByKey() error\n");
					continue;
				}
				if(d_datetime==DAL_DATETIME_NULL)
					continue;
				if(dalDatetime2Str(temp,sizeof(temp),d_datetime,DAL_TYPE_DATE) < 0)
				{
					fprintf(stderr,"[TELCOBASE]dalDatetime2Str() error\n");
				}
				else
				{
					if(i!=0)
						onion_response_printf(res,", ");
					temp[10]='\0';
					onion_response_printf(res,"\"%s\": \"%s\"",key[i],temp);
				}
			}
			else
			{
				printf("[ERROR]Type of '%s' is not supported\n",key[i]);
			}
		}
		onion_response_printf(res," }");
		entry=dalFetchNext(result);
	}
	
	onion_response_printf(res,"]\n}");

	free(key);
	free(type);
	
	return result_count;
}
