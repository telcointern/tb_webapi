#!/bin/bash

# Put request


curl -H "Content-Type: application/json" -X POST http://10.10.1.28:8080/table/user -d '
{
    "id": 10,
    "name": "Bill",
    "city": "Paris"
}
'
curl -H "Content-Type: application/json" -X PUT http://10.10.1.28:8080/table/user?name=Bill -d '
{
    "city": "London",
    "birth": "2002-02-02"
}
'
curl -H "Content-Type: application/json" -X PUT http://10.10.1.28:8080/table/user?id=10 -d '
{
    "city": "Chicago",
    "score": 10.1
}
'
curl -X DELETE http://10.10.1.28:8080/table/user?city=Chicago

echo 
# Get request
curl http://10.10.1.28:8080/memory
curl http://10.10.1.28:8080/daemon
curl http://10.10.1.28:8080/session
curl http://10.10.1.28:8080/replication

# get result of "select * from __SYS_TABLES__;" query and schema info
curl http://10.10.1.28:8080/table/__sys_tables__
curl http://10.10.1.28:8080/table/desc/user

curl http://10.10.1.28:8080/table/rawbuffer/user

curl http://10.10.1.28:8080/table/user
## num of parameter is only one.
curl http://10.10.1.28:8080/table/user?id=1
## invalid parameter
curl http://10.10.1.28:8080/table/user?iid=1

