#include <onion/onion.h>
#include <onion/dict.h>
#include <onion/log.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>

// define handler
onion_connection_status post_hand(void *p, onion_request *req, onion_response *res)
{
	int flag;
	onion_block *ob=NULL;
	char *data=NULL;
	onion_dict *od1=NULL, *od2=NULL;
	

	// check flag
	flag=onion_request_get_flags(req)&OR_METHODS;
	if(flag==OR_POST)
	{
		ob=onion_request_get_data(req);
		if(ob!=NULL)
		{
			// set content-type
			onion_response_set_header(res,"content-type","application/json");
			data=onion_block_data(ob);
			od1=onion_dict_from_json(data);
			onion_dict_add(od1,"add","value",OD_STRING);
			od2=onion_request_get_query_dict(req);
			if(od2!=NULL)
			{
				onion_dict_merge(od1,od2);
			}
			ob=onion_dict_to_json(od1);
			data=onion_block_data(ob);
			onion_response_write0(res,data);
		}
		else
		{
			onion_response_write0(res,"No Data");
		}
	}
	else
	{
		onion_response_write_headers(res);
		onion_response_printf(res,"req_flag:%d\n",onion_request_get_flags(req));
		onion_response_printf(res,"or_flag:%d\n",flag);
		onion_response_write0(res,"wrong flag");
	}

	return OCS_PROCESSED;
}

onion *o=NULL;
static void shutdown_server(int _)
{
	if(o)
		onion_listen_stop(o);
}

int main(int argc, char **argv)
{
	o=onion_new(O_ONE_LOOP);

	signal(SIGINT,shutdown_server);
	signal(SIGTERM,shutdown_server);

	onion_url *urls=onion_root_url(o);

	// add url
	onion_url_add(urls, "^post/?$", post_hand);

	onion_listen(o);
	onion_free(o);

	return 0;
}
