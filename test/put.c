#include <onion/onion.h>
#include <onion/log.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

// define handler
onion_connection_status put_hand(void *p, onion_request *req, onion_response *res)
{
	int flag, fd, n;
	onion_block *ob=NULL;
	char *data=NULL, *filename=NULL;
	onion_dict *od1=NULL, *od2=NULL;
	char str[30];
	

	// check flag
	flag=onion_request_get_flags(req)&OR_METHODS;
	if(flag==OR_PUT)
	{
		strcpy(str,onion_request_get_query(req,"name"));
		ob=onion_request_get_data(req);
		filename=onion_block_data(ob);
		fd=open(filename,O_RDONLY);
		n=lseek(fd,0,SEEK_END);
		if(n!=0)
		{
			// set content-type
			onion_response_set_header(res,"content-type","application/json");
			data=(char*)malloc((sizeof(char)*n)+1);
			lseek(fd,0,SEEK_SET);
			read(fd,data,n);
			data[n]='\0';
			onion_response_write0(res,data);
			free(data);
		}
		else
		{
			onion_response_write0(res,"No Data");
		}
	}
	else
	{
		onion_response_write_headers(res);
		onion_response_printf(res,"req_flag:%d\n",onion_request_get_flags(req));
		onion_response_printf(res,"or_flag:%d\n",flag);
		onion_response_write0(res,"wrong flag");
	}

	return OCS_PROCESSED;
}

onion *o=NULL;
static void shutdown_server(int _)
{
	if(o)
		onion_listen_stop(o);
}

int main(int argc, char **argv)
{
	o=onion_new(O_ONE_LOOP);

	signal(SIGINT,shutdown_server);
	signal(SIGTERM,shutdown_server);

	onion_url *urls=onion_root_url(o);

	// add url
	onion_url_add(urls, "^put/?$", put_hand);

	onion_listen(o);
	onion_free(o);

	return 0;
}
